evenRound = (num, decimalPlaces) ->
  d = decimalPlaces or 0
  m = 10 ** d
  n = +(if d then num * m else num).toFixed(8)
  i = Math.floor(n)
  f = n - i
  e = 1e-8
  r = if f > 0.5 - e and f < 0.5 + e then (if i % 2 is 0 then i else i + 1) else Math.round(n)
  if d then r / m else r

calculatePercentage = (exoticOld) ->
  if exoticOld then 0.7 else 0.8

calculateThreshold = (exoticOld) ->
  if exoticOld then 4 else 6

calculateInfusion = (old, src, threshold, percentage) ->
  oldInt = +old
  srcInt = +src
  if srcInt - oldInt <= threshold
    return srcInt
  else
    return oldInt + evenRound((srcInt - oldInt) * percentage)

(($) ->
  $("#calculate").on "click", ->
    exoticOld = true if $("select[name=itemQualityOld] :selected").val() is 'E'
    threshold = calculateThreshold(exoticOld)
    percentage = calculatePercentage(exoticOld)
    $("input[name=itemLevelNew]").val(calculateInfusion $("input[name=itemLevelOld]").val(), $("input[name=itemLevelSrc]").val(), threshold, percentage)
    $("#useNewSrc").removeAttr("disabled")

  $("#useNewSrc").on "click", ->
    $("input[name=itemLevelOld]").val($("input[name=itemLevelNew]").val())
    $("input[name=itemLevelNew]").val("")
    $("#useNewSrc").attr("disabled", true)
    $("input[name=itemLevelSrc]").val("").focus()
) jQuery
